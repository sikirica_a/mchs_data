# Machine Learning Based Surrogate Models for Microchannel Heat Sink Optimization

This repository contains supplementary data for Pareto fronts described in article **"Machine learning based surrogate models for microchannel heat sink optimization"** by Sikirica, A., Grbčić, L., Kranjčević, L. [[1]](https://arxiv.org/abs/2208.09683).

## Supplementary Files

Supplementary materials are organized as follows:
- pareto_front_CFD_scmc.csv
- pareto_front_CFD_rmc.csv
- pareto_front_ANN_scmc.csv
- pareto_front_ANN_rmc.csv

## How to Cite This Work
Sikirica, A., Grbčić, L., Kranjčević, L., 2022. Machine learning based surrogate models for microchannel heat sink optimization. arXiv preprint arXiv:2208.09683.

<blockquote>

@&#8204;article{sikirica2022machine,\
              author = {Sikirica, Ante and Grbčić, Luka and Kranjčević, Lado},\
              journal = {arXiv preprint arXiv:2208.09683},\
              title = {{Machine learning based surrogate models for microchannel heat sink optimization}},\
              year = {2022},\
              }

</blockquote>
